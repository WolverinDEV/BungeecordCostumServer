package dev.wolveringer.chunk;

import static org.junit.Assert.*;

import org.junit.Test;

import dev.wolveringer.server.chunk.impl.v1_9.v1_9_ChunkImpl;

public class TestChunk_v1_9 {

	@Test
	public void test() {
		int blockId = 55;
		int sid = 11;
		int bitmask = 65535;
		v1_9_ChunkImpl testChunk = new v1_9_ChunkImpl();
		int x = 0;
		while (x < 16) {
			int y = 0;
			while (y < 256) {
				int z = 0;
				while (z < 16) {
					testChunk.setType(x, y, z, blockId);
					testChunk.setMetaData(x, y, z, sid);
					++z;
				}
				++y;
			}
			++x;
		}
		
		byte[] chunkBinaryData = testChunk.createData(bitmask, true, false);
		v1_9_ChunkImpl out = new v1_9_ChunkImpl(chunkBinaryData, bitmask, true, false);
		int right = 0;
		int wrong = 0;
		int x2 = 0;
		while (x2 < 16) {
			int y = 0;
			while (y < 256) {
				int z = 0;
				while (z < 16) {
					if (out.getType(x2, y, z) != blockId || out.getMetaData(x2, y, z) != sid) {
						++wrong;
						System.out.println(out.getType(x2, y, z));
					} else {
						++right;
					}
					++z;
				}
				++y;
			}
			++x2;
		}
		assertTrue("Invalid blocks ("+wrong+")", wrong == 0);
		assertTrue("Invalid right blocks ("+right+")", right == 16*16*256);
	}

}
