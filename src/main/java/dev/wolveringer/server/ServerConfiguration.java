package dev.wolveringer.server;

import java.util.List;

import dev.wolveringer.server.world.World;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class ServerConfiguration {
	private String title;
	private String subTitle;
	private String actionBar;
	private List<String> chat;
	private List<String> tab;

	private World world;

	private int renderDistance;
}
