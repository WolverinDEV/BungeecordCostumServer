package dev.wolveringer.server;

import java.util.HashMap;

import dev.wolveringer.bungeeutil.player.Player;
import dev.wolveringer.server.connection.LocalServerConnection;
import dev.wolveringer.server.world.World;
import lombok.Getter;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;

@Getter
public class CostumServer {
	private static HashMap<Player, CostumServer> server = new HashMap<>();

	public static CostumServer createServer(Plugin pluginInstance, Player player, ServerConfiguration config) {
		CostumServer server = new CostumServer(pluginInstance, player, config);
		CostumServer.server.put(player, server);
		return server;
	}

	public static CostumServer getServer(Player player) {
		return server.get(player);
	}

	private LocalServerConnection connection;
	private Player player;

	private ServerConfiguration config;

	private Plugin pluginInstance;

	public CostumServer(Plugin pluginInstance, Player player, ServerConfiguration config) {
		this.player = player;
		this.pluginInstance = pluginInstance;
		this.config = config;
		this.recreateServer();
	}

	private void recreateServer() {
		this.connection = new LocalServerConnection(this.pluginInstance, (UserConnection) this.player, this.config) {

			@Override
			public void disconnected() {
				server.remove(CostumServer.this.player);
			}
		};
	}

	public void setWorld(World world) {
		this.config.setWorld(world);
		this.recreateServer();
	}

	public void switchTo(ServerInfo info) {
		if (info == null) {
			this.player.disconnect();
			return;
		}
		this.player.connect(info);
	}
}
