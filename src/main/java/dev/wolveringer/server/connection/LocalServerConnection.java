package dev.wolveringer.server.connection;

import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;

import dev.wolveringer.bungeeutil.packets.PacketPlayOutChat;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutEntityProperties;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutKeepAlive;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutPosition;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutSetExperience;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutSpawnPostition;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutUpdateHealth;
import dev.wolveringer.bungeeutil.player.Player;
import dev.wolveringer.bungeeutil.player.connection.IInitialHandler;
import dev.wolveringer.bungeeutil.player.connection.ProtocollVersion;
import dev.wolveringer.bungeeutil.position.BlockPosition;
import dev.wolveringer.server.ServerConfiguration;
import dev.wolveringer.server.bungee.AbstractChannelHandlerContext;
import dev.wolveringer.server.bungee.KeepAliveServerInfo;
import dev.wolveringer.server.chunk.ByteChunk;
import dev.wolveringer.server.chunk.Chunk;
import dev.wolveringer.server.chunk.ChunkImpl;
import dev.wolveringer.server.packets.PacketPlayOutMapChunk;
import dev.wolveringer.server.world.World;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.BungeeServerInfo;
import net.md_5.bungee.ServerConnection;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.score.Objective;
import net.md_5.bungee.api.score.Scoreboard;
import net.md_5.bungee.netty.ChannelWrapper;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.packet.Login;
import net.md_5.bungee.protocol.packet.PluginMessage;
import net.md_5.bungee.protocol.packet.Respawn;
import net.md_5.bungee.protocol.packet.ScoreboardObjective;
import net.md_5.bungee.protocol.packet.Team;

public abstract class LocalServerConnection extends ServerConnection {
	private static final Chunk EMPTY_CHUNK = new Chunk(0, 0, ProtocollVersion.v1_8);
	private static final Unsafe UNSAFE = packet -> { };
	private UserConnection player;
	private boolean active = true;
	private ServerConfiguration cfg;
	private ServerInfo last;
	private Plugin pluginInstance;
	private PacketPlayOutChat actionbarPacket;
	private ChannelWrapper channel;

	public LocalServerConnection(Plugin pluginInstance, UserConnection player, ServerConfiguration sc) {
		super(null, null);
		this.channel = new ChannelWrapper(new AbstractChannelHandlerContext()) {

			@Override
			public void close() {
				LocalServerConnection.this.inactive();
			}

			@Override
			public void write(Object packet) {
			}
		};
		this.pluginInstance = pluginInstance;
		this.player = player;
		ServerConnection old = player.getServer();
		this.player.setServer(this);
		if (old != null) {
			if (old instanceof LocalServerConnection) {
				((LocalServerConnection) old).inactive();
			} else {
				this.last = old.getInfo();
				old.setObsolete(true);
				old.disconnect("");
			}
		}
		this.cfg = sc;
		this.updateActrionBar();
		BungeeCord.getInstance().getScheduler().runAsync(pluginInstance, new Runnable() {
			int id;

			@Override
			public void run() {
				while (LocalServerConnection.this.active
						&& ((IInitialHandler) player.getPendingConnection()).isConnected) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (LocalServerConnection.this.actionbarPacket != null) {
						((Player) LocalServerConnection.this.player)
								.sendPacket(LocalServerConnection.this.actionbarPacket);
					}
					PacketPlayOutKeepAlive p = new PacketPlayOutKeepAlive();
					p.setId(this.id++);
					LocalServerConnection.this.getPlayer().sendPacket(p);
				}
			}
		});
		this.joinServer(this.last != null);
		this.updateTab();
		this.sendSetMessages();
		this.getInfo().addPlayer(this.player);
	}

	@Override
	public /* varargs */ void disconnect(BaseComponent... paramArrayOfBaseComponent) {
		this.inactive();
	}

	@Override
	public void disconnect(BaseComponent paramBaseComponent) {
		this.inactive();
	}

	@Override
	public void disconnect(String paramString) {
		this.inactive();
	}

	public abstract void disconnected();

	@Override
	public InetSocketAddress getAddress() {
		return new InetSocketAddress(25565);
	}

	@Override
	public ChannelWrapper getCh() {
		return this.channel;
	}

	@Override
	public BungeeServerInfo getInfo() {
		return KeepAliveServerInfo.INFO;
	}

	public Player getPlayer() {
		return (Player) this.player;
	}

	private synchronized void inactive() {
		if (!this.active) {
			return;
		}
		System.out.println("Try to deactive");
		this.getInfo().removePlayer(this.player);
		this.active = false;
		System.out.println("CostumServer deactive");
		this.disconnected();
	}

	private void joinServer(boolean flag) {
		final World manager = this.cfg.getWorld();
		if (flag) {
			this.player.getTabListHandler().onServerChange();
			Scoreboard serverScoreboard = this.player.getServerSentScoreboard();
			for (Objective objective : serverScoreboard.getObjectives()) {
				this.player.unsafe().sendPacket(
						new ScoreboardObjective(objective.getName(), objective.getValue(), "integer", (byte) 1));
			}
			for (net.md_5.bungee.api.score.Team team : serverScoreboard.getTeams()) {
				this.player.unsafe().sendPacket(new Team(team.getName()));
			}
			serverScoreboard.clear();
			try {
				Method m = UserConnection.class.getDeclaredMethod("sendDimensionSwitch");
				m.setAccessible(true);
				m.invoke(this.player);
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.player.unsafe().sendPacket(new Respawn(0, (short) 0, (short) 0, "world"));
			this.player.setDimensionChange(false);
			this.player.setServerEntityId(1);
		} else {
			this.player.setClientEntityId(1);
			this.player.setServerEntityId(1);
			Login modLogin = new Login(1, (short) 0, 0, (short) 1,
					(byte) this.player.getPendingConnection().getListener().getTabListSize(), "Default", false);
			this.player.unsafe().sendPacket(modLogin);
			ByteBuf brand = ByteBufAllocator.DEFAULT.heapBuffer();
			DefinedPacket.writeString(
					BungeeCord.getInstance().getName() + " (" + BungeeCord.getInstance().getVersion() + ")", brand);
			this.player.unsafe().sendPacket(new PluginMessage("MC|Brand", brand.array().clone(), false));
			brand.release();
			this.getPlayer()
					.sendPacket(new PacketPlayOutSpawnPostition(new BlockPosition(manager.getWorldSpawn().getBlockX(),
							manager.getWorldSpawn().getBlockY(), manager.getWorldSpawn().getBlockZ())));
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.getPlayer().sendPacket(new PacketPlayOutUpdateHealth(1.0f, 20, 0.0f));
		this.getPlayer().sendPacket(new PacketPlayOutEntityProperties(this.player.getClientEntityId(), new ArrayList<>(
				Arrays.asList(new PacketPlayOutEntityProperties.EntityProperty("generic.maxHealth", 0.0)))));
		this.getPlayer().sendPacket(new PacketPlayOutSetExperience(0f, -10, 11111));
		this.getPlayer().sendPacket(new PacketPlayOutPosition(true, manager.getWorldSpawn(), (byte) 0, 0));
		BungeeCord.getInstance().getScheduler().runAsync(this.pluginInstance,
				() -> LocalServerConnection.this.sendWorldChunks(manager));
	}

	@Override
	public void sendData(String paramString, byte[] paramArrayOfByte) {
	}

	private void sendSetMessages() {
		for (String s : this.cfg.getChat()) {
			this.player.sendMessage(s);
		}
	}

	public void sendWorldChunks(World manager) {
		for (Chunk data : manager.getChunks(manager.getWorldSpawn().getBlockX() >> 4,
				manager.getWorldSpawn().getBlockZ() >> 4, this.cfg.getRenderDistance())) {
			ChunkImpl cdata = data.getImpl() == null ? EMPTY_CHUNK.getImpl() : data.getImpl();
			ByteChunk c = new ByteChunk(data.getLocX(), data.getLocY(), cdata);
			PacketPlayOutMapChunk p = new PacketPlayOutMapChunk();
			p.setSelection(c);
			this.getPlayer().sendPacket(p);
		}
		this.getPlayer().sendPacket(new PacketPlayOutPosition(true, manager.getWorldSpawn(), (byte) 0, 0));
	}

	@Override
	public Connection.Unsafe unsafe() {
		return UNSAFE;
	}

	public void updateActrionBar() {
		if (this.cfg.getActionBar() == null) {
			return;
		}
		this.actionbarPacket = new PacketPlayOutChat();
		this.actionbarPacket.setModus((byte) 2);
		this.actionbarPacket.setRawMessage(("{\"text\":\"" + this.cfg.getActionBar() + "\"}").getBytes());
	}

	public void updateTab() {
		ComponentBuilder builder = new ComponentBuilder("");
		int i = 1;
		for (String s : this.cfg.getTab()) {
			if (i + 1 < this.cfg.getTab().size()) {
				builder.append(s + "\n");
				continue;
			}
			if (i++ >= this.cfg.getTab().size()) {
				continue;
			}
			builder.append(s);
		}
		this.player.setTabHeader(builder.create(),
				new ComponentBuilder(this.cfg.getTab().get(this.cfg.getTab().size() - 1)).create());
	}

	public void updateTitle() {
		BungeeCord.getInstance().createTitle().title(new ComponentBuilder(this.cfg.getTitle()).create())
				.subTitle(new ComponentBuilder(this.cfg.getSubTitle()).create()).fadeIn(0).stay(720000).fadeOut(20)
				.send(this.player);
	}

}
