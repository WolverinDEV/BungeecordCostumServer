package dev.wolveringer.server.world;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;

public class WorldFileReader {
	private static HashMap<File, World> worlds = new HashMap<>();

	public static boolean isWorld(File file) {
		if (worlds.containsKey(file)) {
			return true;
		}
		if (!file.exists()) {
			return false;
		}
		if (file.list((dir, name) -> name.equalsIgnoreCase("level.dat")).length == 0) {
			return false;
		}
		if (new File(file, "region/").listFiles((FilenameFilter) (dir, name) -> name.endsWith(".mca")).length == 0) {
			return false;
		}
		return true;
	}

	public static World read(File file) {
		if (worlds.containsKey(file)) {
			return worlds.get(file);
		}
		if (!file.exists()) {
			throw new RuntimeException("World (" + file.getAbsolutePath() + ") not exist");
		}
		if (file.list((dir, name) -> name.equalsIgnoreCase("level.dat")).length == 0) {
			throw new NullPointerException(String.valueOf(file.getAbsolutePath()) + " isnt a world dir.");
		}
		World manager = new World(new File(file, "level.dat"));
		File[] arrfile = new File(file, "region/").listFiles((FilenameFilter) (dir, name) -> name.endsWith(".mca"));
		int n = arrfile.length;
		int n2 = 0;
		while (n2 < n) {
			File nbtFile = arrfile[n2];
			try {
				MCAFileReader reader = new MCAFileReader(nbtFile);
				manager.getChunks().addChunkFile(reader);
			} catch (Exception e) {
				e.printStackTrace();
			}
			++n2;
		}
		worlds.put(file, manager);
		return manager;
	}

}
