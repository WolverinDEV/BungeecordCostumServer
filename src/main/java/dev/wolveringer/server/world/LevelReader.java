package dev.wolveringer.server.world;

import java.io.File;
import java.io.FileInputStream;

import dev.wolveringer.bungeeutil.position.Location;
import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;

public class LevelReader {
	private NBTTagCompound root;

	public LevelReader(File file) {
		try {
			this.root = NBTCompressedStreamTools.read(new FileInputStream(file)).getCompound("Data");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public NBTTagCompound getRoot() {
		return this.root;
	}

	public Location getWorldSpawn() {
		return new Location(this.root.getInt("SpawnX"), this.root.getInt("SpawnY"), this.root.getInt("SpawnZ"));
	}

	public long getWorldTime() {
		return this.root.getLong("DayTime");
	}

	public boolean isRaining() {
		return this.root.getBoolean("raining");
	}

	public boolean isThundering() {
		return this.root.getBoolean("thundering");
	}
}
