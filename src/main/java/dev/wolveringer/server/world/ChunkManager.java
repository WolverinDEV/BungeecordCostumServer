package dev.wolveringer.server.world;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import dev.wolveringer.nbt.NBTBase;
import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTReadLimiter;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.server.chunk.Chunk;
import dev.wolveringer.server.chunk.NibbleArray3d;
import dev.wolveringer.server.chunk.impl.v1_8.v1_8_ChunkDataSection;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

public class ChunkManager {
	@AllArgsConstructor
	@NoArgsConstructor
	@Getter
	@ToString
	@EqualsAndHashCode
	private static class Location2DKey {
		private int x;
		private int z;
	}

	private HashMap<Location2DKey, MCAFileReader> superChunks = new HashMap<>();

	private HashMap<Location2DKey, Chunk> chunks = new HashMap<>();

	public void addChunkFile(MCAFileReader file) {
		this.superChunks.put(new Location2DKey(file.getLocX(), file.getLocZ()), file);
	}

	private Chunk createChunk(byte[] data) {
		NBTTagCompound comp;
		try {
			comp = NBTCompressedStreamTools.read(new DataInputStream(new ByteArrayInputStream(data)),
					NBTReadLimiter.unlimited);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		dev.wolveringer.server.chunk.impl.v1_8.v1_8_ChunkImpl chunk = new dev.wolveringer.server.chunk.impl.v1_8.v1_8_ChunkImpl();
		int chunkX = comp.getCompound("Level").getInt("xPos");
		int chunkZ = comp.getCompound("Level").getInt("zPos");
		for (NBTBase base_selection : comp.getCompound("Level").getList("Sections").asList()) {
			NBTTagCompound selection = (NBTTagCompound) base_selection;
			byte y = selection.getByte("Y");
			v1_8_ChunkDataSection chunkDataSelection = new v1_8_ChunkDataSection(true);
			byte[] blockIds = selection.getByteArray("Blocks");
			NibbleArray3d blockData = new NibbleArray3d(selection.getByteArray("Data"));
			NibbleArray3d skyLight = new NibbleArray3d(selection.getByteArray("SkyLight"));
			NibbleArray3d blockLight = new NibbleArray3d(selection.getByteArray("BlockLight"));
			char[] blocks = new char[4096];
			int i = 0;
			while (i < blocks.length) {
				blocks[i] = (char) ((blockIds[i] & 255) << 4 | blockData.get(i) & 15);
				++i;
			}
			chunkDataSelection.blockIds = blocks;
			chunkDataSelection.skyLight = skyLight;
			chunkDataSelection.emittedLight = blockLight;
			chunkDataSelection.recalcBlockCounts();
			chunk.getSections()[y] = chunkDataSelection;
		}
		return new Chunk(comp, chunk, chunkX, chunkZ);
	}

	public Chunk getChuk(int chunkX, int chunkZ) {
		Chunk data = this.chunks.get(new Location2DKey(chunkX, chunkZ));
		if (data == null) {
			byte[] byteData;
			MCAFileReader file = this.getFile(chunkX, chunkZ);
			if (file != null && (byteData = file.getChunkData(chunkX & 31, chunkZ & 31)) != null) {
				data = this.createChunk(byteData);
			}
			if (data != null) {
				this.chunks.put(new Location2DKey(chunkX, chunkZ), data);
			}
		}
		if (data == null) {
			data = new Chunk(null, null, chunkX, chunkZ);
		}
		return data;
	}

	public ArrayList<Chunk> getChunks(int x, int z, int r) {
		ArrayList<Chunk> out = new ArrayList<>();
		int dx = -r;
		while (dx < r) {
			int dz = -r;
			while (dz < r) {
				out.add(this.getChuk(x + dx, z + dz));
				++dz;
			}
			++dx;
		}
		return out;
	}

	private MCAFileReader getFile(int chunkX, int chunkZ) {
		return this.superChunks.get(new Location2DKey(chunkX >> 5, chunkZ >> 5));
	}

}
