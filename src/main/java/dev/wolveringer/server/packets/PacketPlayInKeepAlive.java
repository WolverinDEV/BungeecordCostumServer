package dev.wolveringer.server.packets;

import dev.wolveringer.bungeeutil.packetlib.reader.PacketDataSerializer;
import dev.wolveringer.bungeeutil.packets.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PacketPlayInKeepAlive extends Packet {
	private int id;

	@Override
	public void read(PacketDataSerializer arg0) {
		this.id = arg0.readVarInt();
	}

	@Override
	public void write(PacketDataSerializer arg0) {
		arg0.writeVarInt(this.id);
	}
}
