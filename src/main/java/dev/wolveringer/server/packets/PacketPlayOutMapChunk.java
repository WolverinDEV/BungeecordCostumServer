package dev.wolveringer.server.packets;

import java.beans.ConstructorProperties;

import dev.wolveringer.bungeeutil.packetlib.reader.PacketDataSerializer;
import dev.wolveringer.bungeeutil.packetlib.reader.PacketDataSerializer_v1_8;
import dev.wolveringer.bungeeutil.packets.BetaPacket;
import dev.wolveringer.bungeeutil.packets.types.PacketPlayOut;
import dev.wolveringer.bungeeutil.player.ClientVersion;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.server.chunk.ByteChunk;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PacketPlayOutMapChunk extends BetaPacket implements PacketPlayOut {
	private ByteBuf buffer; // do not (de-)serialize all the chunk data
							// unneccessarily
	private ByteChunk selection;
	private NBTTagCompound[] nbtTags = new NBTTagCompound[0];

	@ConstructorProperties(value = { "selection", "nbtTags" })
	public PacketPlayOutMapChunk(ByteChunk selection, NBTTagCompound[] nbtTags) {
		this.selection = selection;
		this.nbtTags = nbtTags;
	}

	public ByteChunk getSelection() {
		return this.selection;
	}

	@Override
	public void read(PacketDataSerializer s) {
		this.buffer = s.slice().retain(); // no memory copy
		s.skipBytes(s.readableBytes());
	}

	public ByteChunk getSection() {
		if (this.selection == null) {
			if (this.buffer != null) {
				PacketDataSerializer s = new PacketDataSerializer_v1_8(this.buffer);
				this.selection = new ByteChunk(s.readInt(), s.readInt(), s.readBoolean(),
						this.getBigVersion() != ClientVersion.BigClientVersion.v1_8 ? s.readVarInt()
								: s.readShort() & 65535,
						this.getVersion().getProtocollVersion(), s.readByteArray(), null);
				if (this.getVersion().getProtocollVersion().ordinal() >= 4) {
					int length = s.readVarInt();
					this.nbtTags = new NBTTagCompound[length];
					int i = 0;
					while (i < length) {
						this.nbtTags[i] = s.readNBT();
						++i;
					}
				}
				s.release();
			}
		}
		return selection;
	}

	public void setSelection(ByteChunk selection) {
		this.selection = selection;
		this.selection.setBiome(true);
	}

	@Override
	public void write(PacketDataSerializer s) {
		if (this.buffer != null) {
			s.writeBytes(this.buffer);
			this.buffer.release();
			return;
		}
		s.writeInt(this.selection.getLocX());
		s.writeInt(this.selection.getLocZ());
		s.writeBoolean(this.selection.isBiome());
		if (this.getBigVersion() != ClientVersion.BigClientVersion.v1_8) {
			s.writeVarInt(this.selection.getBitmask());
		} else {
			s.writeShort((short) (this.selection.getBitmask() & 65535));
		}
		s.writeByteArray(this.selection.getData(this.getVersion().getProtocollVersion()));
		if (this.getVersion().getProtocollVersion().ordinal() >= 4) {
			s.writeVarInt(this.nbtTags.length);
			int i = 0;
			while (i < this.nbtTags.length) {
				s.writeNBT(this.nbtTags[i]);
				++i;
			}
		}
	}
}
