package dev.wolveringer.server.chunk;

import dev.wolveringer.bungeeutil.item.Material;
import dev.wolveringer.bungeeutil.position.BlockPosition;
import dev.wolveringer.nbt.NBTTagCompound;
import lombok.Getter;

@Getter
public class Block {
	private final Chunk chunk;
	private final BlockPosition location;
	private NBTTagCompound blockData;

	public Block(Chunk handle, BlockPosition loc) {
		this.chunk = handle;
		this.location = loc;
		this.blockData = handle.getBlockData(loc);
	}

	public byte getData() {
		return (byte) this.chunk.getMetaData(this.location.getX() % 32, this.location.getY(), this.location.getZ() % 32);
	}

	@SuppressWarnings("deprecation")
	public Material getMaterial() {
		return Material.getMaterial(this.getTypeId());
	}

	public int getTypeId() {
		return this.chunk.getType(this.location.getX() % 32, this.location.getY(), this.location.getZ() % 32);
	}

	public void setData(byte data) {
		this.chunk.setMetaData(this.location.getX() % 32, this.location.getY(), this.location.getZ() % 32, data);
	}

	public void setMaterial(Material m) {
		this.setTypeId(m.getId());
	}

	public void setTypeId(int id) {
		this.chunk.setType(this.location.getX() % 32, this.location.getY(), this.location.getZ() % 32, id);
	}
}
