package dev.wolveringer.server.chunk;

import dev.wolveringer.bungeeutil.player.connection.ProtocollVersion;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class ByteChunk {
	private byte[] data;
	@Getter
	@Setter
	private byte[] biomes = new byte[265];
	@Getter
	private int bitmask;
	@Getter
	private int locX;
	@Getter
	private int locZ;
	@Getter
	@Setter
	private boolean biome;
	@Getter
	private boolean skyLight;
	@Getter
	private ProtocollVersion version;
	private ChunkImpl chunk;

	public ByteChunk(int locX, int locZ, boolean biomes, int bitmask, ProtocollVersion version, byte[] data) {
		this.locX = locX;
		this.locZ = locZ;
		this.data = data;
		this.biome = biomes;
		this.bitmask = bitmask;
		this.version = version;
	}

	public ByteChunk(int locX, int locZ, boolean biomes, int bitmask, ProtocollVersion version, byte[] data,
			byte[] biome) {
		this.locX = locX;
		this.locZ = locZ;
		this.data = data;
		this.biome = biomes;
		this.bitmask = bitmask;
		this.version = version;
		this.biomes = biome;
	}

	public ByteChunk(int locX, int locZ, ChunkImpl chunk) {
		this.locX = locX;
		this.locZ = locZ;
		this.chunk = chunk;
		this.version = chunk.getVersion();
		this.bitmask = chunk.calculateBitmask(false);
		this.biome = false;
		this.skyLight = true;
	}

	public void convertTo(ProtocollVersion version) {
		if (version != this.version) {
			System.out.println("Convert not supported!");
		}
	}

	public ChunkImpl getChunk() {
		if (this.chunk == null) {
			this.chunk = ChunkImplementationRegistry.getInstance().createData(this.version, this.data, this.biomes,
					this.bitmask, this.skyLight, this.biome);
		}
		return this.chunk;
	}

	public byte[] getData() {
		if (this.data == null) {
			this.data = this.chunk.createData(this.bitmask, this.skyLight, this.biome);
		}
		return this.data;
	}

	public byte[] getData(ProtocollVersion version) {
		if (version == this.version) {
			if (this.data == null) {
				this.data = this.chunk.createData(this.bitmask, this.skyLight, this.biome);
			}
			return this.data;
		}
		return this.chunk.convertTo(version).createData(this.bitmask, this.skyLight, this.biome);
	}

	public int getDataSize() {
		return this.getData().length;
	}
}
