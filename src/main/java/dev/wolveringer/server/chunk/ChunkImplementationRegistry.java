package dev.wolveringer.server.chunk;

import java.util.EnumMap;

import dev.wolveringer.bungeeutil.player.connection.ProtocollVersion;

public class ChunkImplementationRegistry {
	private static interface ConstructorInvoker {
		public ChunkImpl createNewInstance();

		public ChunkImpl createNewInstance(byte[] var1, byte[] var2, int var3, boolean var4, boolean var5);
	}

	private static ChunkImplementationRegistry instance = new ChunkImplementationRegistry();

	private static final ConstructorInvoker CI_18 = new ConstructorInvoker() {
		@Override
		public ChunkImpl createNewInstance() {
			return new dev.wolveringer.server.chunk.impl.v1_8.v1_8_ChunkImpl();
		}

		@Override
		public ChunkImpl createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight,
				boolean biomes) {
			return new dev.wolveringer.server.chunk.impl.v1_8.v1_8_ChunkImpl(data, bitmask, skyLight, biomes);
		}
	};
	private static final ConstructorInvoker CI_19 = new ConstructorInvoker() {
		@Override
		public ChunkImpl createNewInstance() {
			return new dev.wolveringer.server.chunk.impl.v1_9.v1_9_ChunkImpl();
		}

		@Override
		public ChunkImpl createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight,
				boolean biomes) {
			return new dev.wolveringer.server.chunk.impl.v1_9.v1_9_ChunkImpl(data, bitmask, skyLight, biomes);
		}
	};
	
	static {
		ChunkImplementationRegistry.instance.cons.put(ProtocollVersion.v1_8, CI_18);
		ChunkImplementationRegistry.instance.cons.put(ProtocollVersion.v1_9, CI_19);
		ChunkImplementationRegistry.instance.cons.put(ProtocollVersion.v1_9_2, CI_19);
		ChunkImplementationRegistry.instance.cons.put(ProtocollVersion.v1_9_3, CI_19);
		ChunkImplementationRegistry.instance.cons.put(ProtocollVersion.v1_9_4, CI_19);
		ChunkImplementationRegistry.instance.cons.put(ProtocollVersion.v1_10, CI_19);
		ChunkImplementationRegistry.instance.cons.put(ProtocollVersion.v1_11, CI_19);
	}

	public static ChunkImplementationRegistry getInstance() {
		return instance;
	}

	public static void setInstance(ChunkImplementationRegistry instance) {
		ChunkImplementationRegistry.instance = instance;
	}

	private EnumMap<ProtocollVersion, ConstructorInvoker> cons = new EnumMap<>(ProtocollVersion.class);

	public ChunkImpl createData(ProtocollVersion version) {
		if (this.cons.get(version) == null) {
			System.out.println("Cant create chunkdata for " + version);
			return null;
		}
		return this.cons.get(version).createNewInstance();
	}

	public ChunkImpl createData(ProtocollVersion version, byte[] data, byte[] biome, int bitmask, boolean skyLight,
			boolean biomes) {
		if (this.cons.get(version) == null) {
			System.out.println("Cant create chunkdata with data for " + version);
			return null;
		}
		return this.cons.get(version).createNewInstance(data, biome, bitmask, skyLight, biomes);
	}

}
