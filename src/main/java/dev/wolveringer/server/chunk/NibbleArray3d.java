package dev.wolveringer.server.chunk;

import java.util.Arrays;

import dev.wolveringer.bungeeutil.packetlib.reader.PacketDataSerializer;

public class NibbleArray3d {
	private byte[] data;

	public NibbleArray3d(byte[] array) {
		this.data = array;
	}

	public NibbleArray3d(int size) {
		this.data = new byte[size >> 1];
	}

	public NibbleArray3d(PacketDataSerializer in, int size) {
		this.data = new byte[size];
		in.readBytes(size / 2);
	}
	
	public NibbleArray3d(char[] idArray) {
		this.data = new byte[idArray.length * 2];
		int i = 0;
		int n = idArray.length;
		int n2 = 0;
		while (n2 < n) {
			char c = idArray[n2];
			this.data[i++] = (byte) ((byte) c & 255);
			this.data[i++] = (byte) ((byte) (c >> 8) & 255);
			++n2;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (!(this == o || o instanceof NibbleArray3d && Arrays.equals(this.data, ((NibbleArray3d) o).data))) {
			return false;
		}
		return true;
	}

	public void fill(int val) {
		int index = 0;
		while (index < this.data.length << 1) {
			int ind = index >> 1;
			int part = index & 1;
			this.data[ind] = part == 0 ? (byte) (this.data[ind] & 240 | val & 15)
					: (byte) (this.data[ind] & 15 | (val & 15) << 4);
			++index;
		}
	}

	public int get(int i) {
		int index = i >> 1;
		int part = i & 1;
		return part == 0 ? this.data[index] & 15 : this.data[index] >> 4 & 15;
	}
	
	public int get(int x, int y, int z) {
		int key = (y & 15) << 8 | z << 4 | x;
		return get(key);
	}

	public byte[] getData() {
		return this.data;
	}
	
	public int getDataSize(){
		return this.data.length;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.data);
	}

	public void set(int x, int y, int z, int val) {
		int key = (y & 15) << 8 | z << 4 | x;
		int index = key >> 1;
		int part = key & 1;
		this.data[index] = part == 0 ? (byte) (this.data[index] & 240 | val & 15)
				: (byte) (this.data[index] & 15 | (val & 15) << 4);
	}

	public void write(PacketDataSerializer out) {
		out.writeBytes(this.data);
	}
}
