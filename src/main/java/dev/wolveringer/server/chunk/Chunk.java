package dev.wolveringer.server.chunk;

import java.util.ArrayList;
import java.util.HashMap;

import dev.wolveringer.bungeeutil.packets.PacketPlayOutUpdateSign;
import dev.wolveringer.bungeeutil.packets.types.PacketPlayOut;
import dev.wolveringer.bungeeutil.player.connection.ProtocollVersion;
import dev.wolveringer.bungeeutil.position.BlockPosition;
import dev.wolveringer.nbt.NBTBase;
import dev.wolveringer.nbt.NBTTagCompound;
import lombok.Getter;

@Getter
public class Chunk {
	private ChunkImpl impl;
	private int locX;
	private int locY;
	private NBTTagCompound data;
	private byte[] biomes = new byte[256];
	private HashMap<BlockPosition, NBTTagCompound> blockData = new HashMap<>();

	public Chunk(int locX, int locY, ProtocollVersion version) {
		this.locX = locX;
		this.locY = locX;
		this.impl = ChunkImplementationRegistry.getInstance().createData(version);
	}

	public Chunk(NBTTagCompound data, ChunkImpl handle, int locX, int locY) {
		this.data = data;
		this.impl = handle;
		this.locX = locX;
		this.locY = locY;
		if (data != null && data.getCompound("Level").hasKey("Biomes")) {
			this.biomes = data.getCompound("Level").getByteArray("Biomes");
		}
		this.loadBlockData();
	}

	public NBTTagCompound getBlockData(BlockPosition loc) {
		return this.blockData.get(loc);
	}

	public byte getBlockLight(int x, int y, int z) {
		return this.impl.getBlockLight(x, y, z);
	}
	
	public int getMetaData(int x, int y, int z) {
		return this.impl.getMetaData(x, y, z);
	}

	public byte getSkyLight(int x, int y, int z) {
		return this.impl.getSkyLight(x, y, z);
	}

	public int getType(int x, int y, int z) {
		return this.impl.getType(x, y, z);
	}

	public ArrayList<PacketPlayOut> getUpdatePackets() {
		ArrayList<PacketPlayOut> out = new ArrayList<>();
		for (BlockPosition pos : this.blockData.keySet()) {
			NBTTagCompound comp = this.blockData.get(pos);
			String type = comp.getString("id");
			if (!type.equalsIgnoreCase("Sign")) {
				continue;
			}
			out.add(new PacketPlayOutUpdateSign(pos, new String[] { comp.getString("Text1"), comp.getString("Text2"),
					comp.getString("Text3"), comp.getString("Text4") }));
		}
		return out;
	}

	private void loadBlockData() {
		if (this.data != null) {
			for (NBTBase base : this.data.getCompound("Level").getList("TileEntities").asList()) {
				NBTTagCompound nbt = (NBTTagCompound) base;
				this.blockData.put(new BlockPosition(nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z")), nbt);
			}
		}
	}

	public void setBlockLight(int x, int y, int z, int blockLight) {
		this.impl.setBlockLight(x, y, z, blockLight);
	}

	public void setMetaData(int x, int y, int z, int metaData) {
		this.impl.setMetaData(x, y, z, metaData);
	}

	public void setSkyLight(int x, int y, int z, int skyLight) {
		this.impl.setSkyLight(x, y, z, skyLight);
	}

	public void setType(int x, int y, int z, int type) {
		this.impl.setType(x, y, z, type);
	}
}
