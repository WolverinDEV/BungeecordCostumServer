package dev.wolveringer.server.chunk.impl.v1_8;

import dev.wolveringer.server.chunk.NibbleArray3d;

public class v1_8_ChunkDataSection {
	public static final int WIDTH = 16;
	public static final int HEIGHT = 16;
	public static final int DEPTH = 256;
	private static final int SEC_DEPTH = 16;
	private static final int ARRAY_SIZE = 4096;
	public int nonEmptyBlockCount;
	public char[] blockIds;
	public NibbleArray3d emittedLight;
	public NibbleArray3d skyLight;

	public v1_8_ChunkDataSection(boolean skyLight) {
		this.blockIds = new char[4096];
		this.emittedLight = new NibbleArray3d(4096);
		if (skyLight) {
			this.skyLight = new NibbleArray3d(4096);
		}
	}

	public v1_8_ChunkDataSection(boolean skyLight, char[] blockIds) {
		this.blockIds = blockIds;
		this.emittedLight = new NibbleArray3d(4096);
		if (skyLight) {
			this.skyLight = new NibbleArray3d(4096);
		}
		this.recalcBlockCounts();
	}

	public int getBlockLight(int dx, int dy, int dz) {
		return this.emittedLight.get(dx, dy, dz);
	}

	public NibbleArray3d getEmittedLightArray() {
		return this.emittedLight;
	}

	public char[] getIdArray() {
		return this.blockIds;
	}

	public int getSkyLight(int dx, int dy, int dz) {
		return this.skyLight.get(dx, dy, dz);
	}

	public NibbleArray3d getSkyLightArray() {
		return this.skyLight;
	}

	public int getType(int dx, int dy, int dz) {
		return this.blockIds[this.index(dx, dy, dz)];
	}

	public int index(int x, int y, int z) {
		if (x < 0 || z < 0 || x >= 16 || z >= 16) {
			throw new IndexOutOfBoundsException("Coords (x=" + x + ",z=" + z + ") out of section bounds");
		}
		return (y & 15) << 8 | z << 4 | x;
	}

	public boolean isEmpty() {
		if (this.nonEmptyBlockCount == 0) {
			return true;
		}
		return false;
	}

	public void recalcBlockCounts() {
		this.nonEmptyBlockCount = 0;
		int x = 0;
		while (x < 16) {
			int y = 0;
			while (y < 16) {
				int z = 0;
				while (z < 16) {
					int block = this.getType(x, y, z);
					if (block != 0) {
						++this.nonEmptyBlockCount;
					}
					++z;
				}
				++y;
			}
			++x;
		}
	}

	public void setBlockLight(int dx, int dy, int dz, int value) {
		this.emittedLight.set(dx, dy, dz, value);
	}

	public void setBlockLight(NibbleArray3d nibblearray) {
		this.emittedLight = nibblearray;
	}

	public void setSkyLight(int dx, int dy, int dz, int value) {
		this.skyLight.set(dx, dy, dz, value);
	}

	public void setSkyLight(NibbleArray3d nibblearray) {
		this.skyLight = nibblearray;
	}

	public void setType(int dx, int dy, int dz, int type) {
		int old_type = this.getType(dx, dy, dz);
		if (old_type != 0) {
			--this.nonEmptyBlockCount;
		}
		if (type != 0) {
			++this.nonEmptyBlockCount;
		}
		this.blockIds[this.index(dx, dy, dz)] = (char) type;
	}
}
