package dev.wolveringer.server.chunk.impl.v1_9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dev.wolveringer.bungeeutil.packetlib.reader.PacketDataSerializer;
import lombok.Getter;

public class FlexibleBlockStorage {
	private static final BlockState AIR = new BlockState(0, 0);

	private static int index(int x, int y, int z) {
		return (y & 15) << 8 | z << 4 | x;
	}

	private static BlockState rawToState(int raw) {
		return new BlockState(raw >> 4, raw & 15);
	}

	private static int stateToRaw(BlockState state) {
		return state.getId() << 4 | state.getData() & 15;
	}

	@Getter
	private int bitsPerEntry;

	private List<BlockState> states;

	@Getter
	private FlexibleStorage storage;

	public FlexibleBlockStorage() {
		this.bitsPerEntry = 4;
		this.states = new ArrayList<>();
		this.states.add(AIR);
		this.storage = new FlexibleStorage(this.bitsPerEntry, 4096);
	}

	public FlexibleBlockStorage(PacketDataSerializer in) {
		long[] data;
		this.bitsPerEntry = in.readUnsignedByte();
		if (this.bitsPerEntry <= 8) {
			this.states = new ArrayList<>();
			int stateCount = in.readVarInt();
			int i = 0;
			while (i < stateCount) {
				this.states.add(new BlockState(in));
				++i;
			}
		}
		if ((data = this.readLongArray(in)).length == 0 || this.bitsPerEntry == 0) {
			System.out.println("Empty chunk? " + data.length + " (" + this.bitsPerEntry + ")");
		}
		this.storage = new FlexibleStorage(this.bitsPerEntry, data);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof FlexibleBlockStorage && this.bitsPerEntry == ((FlexibleBlockStorage) o).bitsPerEntry
				&& this.states.equals(((FlexibleBlockStorage) o).states) && this.storage.equals(((FlexibleBlockStorage) o).storage)) {
			return true;
		}
		return false;
	}

	public BlockState get(int x, int y, int z) {
		int id = this.storage.get(FlexibleBlockStorage.index(x, y, z));
		return this.bitsPerEntry <= 8 ? id >= 0 && id < this.states.size() ? this.states.get(id) : AIR
				: FlexibleBlockStorage.rawToState(id);
	}

	public List<BlockState> getStates() {
		return Collections.unmodifiableList(this.states);
	}
	
	@Override
	public int hashCode() {
		int result = this.bitsPerEntry;
		result = 31 * result + this.states.hashCode();
		result = 31 * result + this.storage.hashCode();
		return result;
	}

	public boolean isEmpty() {
		int index = 0;
		while (index < this.storage.getSize()) {
			if (this.storage.get(index) != 0) {
				return false;
			}
			++index;
		}
		return true;
	}

	private long[] readLongArray(PacketDataSerializer in) {
		long[] array = new long[in.readVarInt()];
		int i = 0;
		while (i < array.length) {
			array[i] = in.readLong();
			++i;
		}
		return array;
	}

	public void set(int x, int y, int z, BlockState state) {
		int id = this.bitsPerEntry <= 8 ? this.states.indexOf(state) : FlexibleBlockStorage.stateToRaw(state);
		if (id == -1) {
			this.states.add(state);
			if (this.states.size() > 1 << this.bitsPerEntry) {
				++this.bitsPerEntry;
				List<BlockState> oldStates = this.states;
				if (this.bitsPerEntry > 8) {
					oldStates = new ArrayList<>(this.states);
					this.states.clear();
					this.bitsPerEntry = 13;
				}
				FlexibleStorage oldStorage = this.storage;
				this.storage = new FlexibleStorage(this.bitsPerEntry, this.storage.getSize());
				int index = 0;
				while (index < this.storage.getSize()) {
					this.storage.set(index, this.bitsPerEntry <= 8 ? oldStorage.get(index)
							: FlexibleBlockStorage.stateToRaw(oldStates.get(index)));
					++index;
				}
			}
			id = this.bitsPerEntry <= 8 ? this.states.indexOf(state) : FlexibleBlockStorage.stateToRaw(state);
		}
		this.storage.set(FlexibleBlockStorage.index(x, y, z), id);
	}

	public void writeData(PacketDataSerializer out) {
		out.writeByte(this.bitsPerEntry);
		if (this.bitsPerEntry <= 8) {
			out.writeVarInt(this.states.size());
			for (BlockState state : this.states) {
				state.write(out);
			}
		}
		long[] data = this.storage.getData();
		this.writeLongArray(out, data);
	}

	private void writeLongArray(PacketDataSerializer out, long[] array) {
		out.writeVarInt(array.length);
		long[] arrl = array;
		int n = arrl.length;
		int n2 = 0;
		while (n2 < n) {
			long l = arrl[n2];
			out.writeLong(l);
			++n2;
		}
	}
}
