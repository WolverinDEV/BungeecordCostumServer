package dev.wolveringer.server.chunk.impl.v1_9;

import dev.wolveringer.bungeeutil.packetlib.reader.PacketDataSerializer;
import dev.wolveringer.server.chunk.NibbleArray3d;

public class v1_9_ChunkSelection {
	public static final int WIDTH = 16;
	public static final int HEIGHT = 16;
	public static final int DEPTH = 16;
	private FlexibleBlockStorage blocks;
	private NibbleArray3d blocklight;
	private NibbleArray3d skylight;

	public v1_9_ChunkSelection(FlexibleBlockStorage blocks, NibbleArray3d blocklight, NibbleArray3d skylight) {
		this.blocks = blocks;
		this.blocklight = blocklight;
		this.skylight = skylight;
	}

	public v1_9_ChunkSelection(boolean skylight) {
		this(new FlexibleBlockStorage(), new NibbleArray3d(4096), skylight ? new NibbleArray3d(4096) : null);
	}

	public v1_9_ChunkSelection(boolean skylight, PacketDataSerializer in) {
		this.blocks = new FlexibleBlockStorage(in);
		this.blocklight = new NibbleArray3d(in, 4096);
		if (skylight) {
			this.skylight = new NibbleArray3d(in, 4096);
		}
	}

	public NibbleArray3d getBlockLight() {
		return this.blocklight;
	}

	public FlexibleBlockStorage getBlocks() {
		return this.blocks;
	}

	public NibbleArray3d getSkyLight() {
		return this.skylight;
	}

	public boolean isEmpty() {
		return this.blocks.isEmpty();
	}

	public void writeData(PacketDataSerializer out) {
		this.blocks.writeData(out);
		this.blocklight.write(out);
		if (this.skylight != null) {
			this.skylight.write(out);
		}
	}
}
