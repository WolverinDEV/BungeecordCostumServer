package dev.wolveringer.server.chunk.impl.v1_8;

import org.apache.commons.lang3.Validate;

import dev.wolveringer.bungeeutil.player.connection.ProtocollVersion;
import dev.wolveringer.server.chunk.ChunkImpl;
import dev.wolveringer.server.chunk.ChunkImplementationRegistry;
import dev.wolveringer.server.chunk.NibbleArray3d;
import lombok.Getter;

public final class v1_8_ChunkImpl implements ChunkImpl {
	@Getter
	private v1_8_ChunkDataSection[] sections = new v1_8_ChunkDataSection[16];

	private byte[] biomes = new byte[256];

	public v1_8_ChunkImpl() {
		int i = 0;
		while (i < this.sections.length) {
			this.sections[i] = new v1_8_ChunkDataSection(true);
			++i;
		}
	}

	public v1_8_ChunkImpl(byte[] data, int bitmask, boolean skyLight, boolean biomes) {
		for(int i = 0;i<sections.length;i++){
			if ((bitmask >> i & 1) == 1) {
				this.sections[i] = new v1_8_ChunkDataSection(skyLight);
			}
		}

		int readerPosition = 0;
		int blocksLength = 4096;
		int lightLength = 2048;
		int skylightLength = skyLight ? 2048 : 0;
		
		//Block id/ids
		for(int dy = 0;dy < sections.length; dy++){
			if ((bitmask >> dy & 1) == 1) {
				char[] blocks = new char[blocksLength];
				for(int i = 0;i<blocksLength;i++){
					byte low = data[readerPosition++];
					byte hight = data[readerPosition++];
					blocks[i] = (char) ((char) (hight << 8) | low & 255);
				}
				this.sections[dy].blockIds = blocks;
				this.sections[dy].recalcBlockCounts();
			}
		}
	
		//Block light
		for(int dy = 0;dy < sections.length; dy++){
			if ((bitmask >> dy & 1) == 1) {
				byte[] light = new byte[lightLength];
				System.arraycopy(data, readerPosition, light, 0, lightLength);
				readerPosition += lightLength;
				this.sections[dy].setBlockLight(new NibbleArray3d(light));
			}
		}
		
		if (skyLight) {
			//Block sky light
			for(int dy = 0;dy < sections.length; dy++){
				if ((bitmask >> dy & 1) == 1) {
					byte[] skylight = new byte[skylightLength];
					System.arraycopy(data, readerPosition, skylight, 0, skylightLength);
					readerPosition += skylightLength;
					this.sections[dy].setSkyLight(new NibbleArray3d(skylight));
				}
			}
		}
		
		this.biomes = new byte[256];
		if (biomes) {
			System.arraycopy(data, readerPosition, this.biomes, 0, 256);
			readerPosition += 256;
		}
		if (data.length != readerPosition) {
			System.out.println("Read to short. Avariable bytes: " + (data.length - readerPosition));
		}
	}

	@Override
	public int calculateBitmask(boolean ignoreEmpty) {
		int mask = 0;
		int i = 0;
		while (i < this.sections.length) {
			if (this.sections[i] != null && (ignoreEmpty || !this.sections[i].isEmpty())) {
				mask |= 1 << i;
			}
			++i;
		}
		return mask;
	}

	public int calculateChunkSize(int chunks, boolean skylight, boolean biomes) {
		int chunkSize = chunks * 2 * 16 * 16 * 16;
		int blockLightSize = chunks * 16 * 16 * 16 / 2;
		int skylightSize = skylight ? chunks * 16 * 16 * 16 / 2 : 0;
		int biomeSize = biomes ? 256 : 0;
		return chunkSize + blockLightSize + skylightSize + biomeSize;
	}

	@Override
	public dev.wolveringer.server.chunk.ChunkImpl convertTo(ProtocollVersion version) {
		if (version == this.getVersion()) {
			return this;
		}
		dev.wolveringer.server.chunk.ChunkImpl _new = ChunkImplementationRegistry.getInstance().createData(version);
		this.transfareBlocks(this, _new);
		return _new;
	}

	@Override
	public int countActiveChunks(boolean ignoreEmpty) {
		return Integer.bitCount(calculateBitmask(ignoreEmpty));
	}

	@Override
	public byte[] createData(int bitmask, boolean skyLight, boolean biome) {
		NibbleArray3d nibblearray;
		int writerPosition = 0;
		
		v1_8_ChunkDataSection[] selections = getSections();
		byte[] data = new byte[this.calculateChunkSize(Integer.bitCount(bitmask), skyLight, biome)];
		
		//Block id/sid
		for(int secIndex = 0;secIndex<selections.length;secIndex++){
			if ((bitmask >> secIndex & 1) == 1) {
				char[] blockIdArray = selections[secIndex].getIdArray();
				for(int index = 0;index < blockIdArray.length; index++){
					data[writerPosition++] = (byte) (blockIdArray[index] & 255);
					data[writerPosition++] = (byte) ((blockIdArray[index] >> 8) & 255);
				}
			}
		}
		
		//Block light
		for(int secIndex = 0;secIndex<selections.length;secIndex++){
			if ((bitmask >> secIndex & 1) == 1) {
				nibblearray = selections[secIndex].getEmittedLightArray();
				System.arraycopy(nibblearray.getData(), 0, data, writerPosition, nibblearray.getDataSize());
				writerPosition += nibblearray.getDataSize();
			}
		}
	
		if (skyLight) {
			//Block sky light
			for(int secIndex = 0;secIndex<selections.length;secIndex++){
				if ((bitmask >> secIndex & 1) == 1) {
					nibblearray = selections[secIndex].getSkyLightArray();
					System.arraycopy(nibblearray.getData(), 0, data, writerPosition, nibblearray.getDataSize());
					writerPosition += nibblearray.getDataSize();
				}
			}
		}
		
		if (biome) {
			Validate.isTrue(this.biomes.length == 256,"Invalid biome size ("+this.biomes.length+" != 256)");
			System.arraycopy(this.biomes, 0, data, writerPosition, this.biomes.length);
			writerPosition += this.biomes.length;
		}
		
		if (writerPosition != data.length) 
			throw new RuntimeException("Writer position didnt reached excpected position. (WriterIndex: "+writerPosition+", Expected "+data.length+")");
		return data;
	}

	@Override
	public byte getBlockLight(int x, int y, int z) {
		v1_8_ChunkDataSection section = this.getSection(y);
		return (byte) (section == null ? 0 : section.emittedLight.get(x, y, z));
	}

	public int getDataSize(int bitmask, boolean skyLight, boolean biome) {
		return this.calculateChunkSize(Integer.bitCount(bitmask), skyLight, biome);
	}

	@Override
	public int getMetaData(int x, int y, int z) {
		v1_8_ChunkDataSection section = this.getSection(y);
		return section == null ? 0 : section.blockIds[section.index(x, y, z)] & 15;
	}

	private v1_8_ChunkDataSection getSection(int y) {
		int idx = y >> 4;
		if (y < 0 || y >= 256 || idx >= this.sections.length)
			return null;
		return this.sections[idx];
	}

	@Override
	public byte getSkyLight(int x, int y, int z) {
		v1_8_ChunkDataSection section = this.getSection(y);
		return (byte) (section == null ? 0 : section.skyLight.get(x, y, z));
	}

	@Override
	public int getType(int x, int y, int z) {
		v1_8_ChunkDataSection section = this.getSection(y);
		return section == null ? 0 : section.blockIds[section.index(x, y, z)] >> 4;
	}

	@Override
	public ProtocollVersion getVersion() {
		return ProtocollVersion.v1_8;
	}

	@Override
	public void setBlockLight(int x, int y, int z, int blockLight) {
		v1_8_ChunkDataSection section = this.getSection(y);
		if (section == null) {
			return;
		}
		section.emittedLight.set(x, y, z, (byte) blockLight);
	}

	@Override
	public void setMetaData(int x, int y, int z, int metaData) {
		if (metaData < 0 || metaData >= 16) {
			throw new IllegalArgumentException("Metadata out of range: " + metaData);
		}
		v1_8_ChunkDataSection section = this.getSection(y);
		if (section == null)
			return;
		
		int index = section.index(x, y, z);
		char type = section.blockIds[index];
		if (type == 0) //Block air
			return;
		
		section.blockIds[index] = (char) (type & 65520 | metaData);
	}

	@Override
	public void setSkyLight(int x, int y, int z, int skyLight) {
		v1_8_ChunkDataSection section = this.getSection(y);
		if (section == null)
			return;
		section.skyLight.set(x, y, z, (byte) skyLight);
	}

	@Override
	public void setType(int x, int y, int z, int type) {
		int idx = y >> 4;
		if (y < 0 || y >= 256 || idx >= this.sections.length)
			return;
		
		if (type < 0 || type >= 256)
			throw new IllegalArgumentException("Block type out of range: " + type);
		
		
		v1_8_ChunkDataSection section = this.getSection(y);
		if (section == null) {
			if (type == 0)
				return;
			this.sections[idx] = section = new v1_8_ChunkDataSection(true);
		}
		
		int index = section.index(x, y, z);
		if (type == 0) {
			if (section.blockIds[index] != 0) {
				--section.nonEmptyBlockCount;
			}
		} else if (section.blockIds[index] == 0) {
			++section.nonEmptyBlockCount;
		}
		
		section.blockIds[index] = (char) (type << 4);
		if (type == 0 && section.nonEmptyBlockCount == 0) {
			this.sections[idx] = null;
		}
	}
}
