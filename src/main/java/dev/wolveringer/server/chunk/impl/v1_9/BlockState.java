package dev.wolveringer.server.chunk.impl.v1_9;

import dev.wolveringer.bungeeutil.packetlib.reader.PacketDataSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class BlockState {
	private int id;
	private int data;
	
	public BlockState(PacketDataSerializer in) {
		int data = in.readVarInt();
		this.id = data >> 4;
		this.data = data & 15;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof BlockState && this.id == ((BlockState) o).id && this.data == ((BlockState) o).data) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int result = this.id;
		result = 31 * result + this.data;
		return result;
	}

	public void write(PacketDataSerializer serelizer) {
		if (this.id != 0) {
			serelizer.writeVarInt(this.id << 4 | this.data & 15);
		} else {
			serelizer.writeVarInt(this.id << 4 | this.data & 15);
		}
	}
}
